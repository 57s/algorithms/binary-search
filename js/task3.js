//(*) Найти ближайший элемент к заданному значению
const arr = [1, 10, 10, 30, 30, 30, 40, 40, 50];
//					[0,  1,  2,  3,  4,  5,  6,  7,  8];

const search = function (arr, item) {
	let left = 0;
	let right = arr.length - 1;

	while (left <= right) {
		let mid = Math.floor((left + right) / 2);

		if (item === arr[mid]) return arr[mid];

		if (item > arr[mid]) left = mid + 1;
		if (item < arr[mid]) right = mid - 1;
	}

	if (right < 0) return arr[0];
	else if (left >= arr.length) return arr.at(-1);
	else {
		const rightIsLess = item - arr[right] < arr[left] - item;
		return rightIsLess ? arr[right] : arr[left];
	}
};

console.log(search(arr, 0));
