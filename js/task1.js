//(*) Найти последнее вхождение в отсортированном массиве
const arr = [1, 10, 10, 30, 30, 30, 40, 40, 50];
//					[0,  1,  2,  3,  4,  5,  6,  7,  8];

const search = function (arr, item) {
	let lastIndex = null;
	let left = 0;
	let right = arr.length - 1;

	while (left <= right) {
		let mid = Math.floor((left + right) / 2);

		if (item === arr[mid]) {
			lastIndex = mid;
			left = mid + 1;
		}

		if (item > arr[mid]) left = mid + 1;
		if (item < arr[mid]) right = mid - 1;
	}

	return lastIndex;
};

console.log(search(arr, 50));
