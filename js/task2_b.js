// Найти заданный элемент,
// или первый элемент меньше заданного
// в отсортированном массиве

const arr = [1, 10, 10, 30, 30, 30, 40, 40, 50];
//					[0,  1,  2,  3,  4,  5,  6,  7,  8];

const search = function (arr, item) {
	let left = 0;
	let right = arr.length - 1;
	let result = null;

	while (left <= right) {
		let mid = Math.floor((left + right) / 2);

		if (arr[mid] <= item) {
			result = arr[mid];
			left = mid + 1;
		} else {
			right = mid - 1;
		}
	}

	return result;
};

console.log(search(arr, 9));
