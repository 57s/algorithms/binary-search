// отгадывает число пользователя за n попыток
const config = {
	min: 1,
	max: 1024,
};

const state = {
	attempts: Math.log2(config.max),
	min: config.min,
	max: config.max,
};

const ui = {
	show(mess) {
		alert(mess);
	},

	answer(question) {
		return prompt(question);
	},

	ask(question) {
		return confirm(question);
	},
};

function showGreetings() {
	ui.show(`
			Нужно загадать число от
			${config.min} до ${config.max}.
			Игра отгадает число максимум за ${state.attempts} попыток.
		`);
}

function getNumber() {
	const answer = ui.answer(`
		Загадай число от
		${config.min} до ${config.max}
	`);
	const userNumber = Number(answer);

	if (answer === null) return ui.show(`Игра отменена`);

	switch (true) {
		case isNaN(userNumber):
			ui.show(`Введенное значение не число!`);
			return getNumber();
		case userNumber < config.min || userNumber > config.max:
			ui.show(`Загаданное число находится все диапазона`);
			return getNumber();
	}

	return userNumber;
}

function game() {
	while (state.attempts >= 0) {
		state.attempts--;
		state.mid = Math.floor(state.max - (state.max - state.min) / 2);
		const isRight = ui.ask(`
			Загаданное число ${state.mid}?
			Осталось попыток ${state.attempts}
		`);

		if (state.attempts <= 0) return ui.show('Попытки кончались');
		if (isRight) {
			return ui.show('Ура');
		} else {
			const isMiddleBig = ui.ask(`Загаданное число меньше  ${state.mid}?`);
			console.log(state);
			if (isMiddleBig) {
				state.max = state.mid - 1;
			} else {
				state.min = state.mid + 1;
			}
		}
	}
}

function start() {
	showGreetings();
	state.num = getNumber();
	game();
}

start();
