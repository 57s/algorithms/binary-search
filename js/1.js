const arr = [1, 2, 3, 4, 5, 6, 7];

function binarySearch(arr, target) {
	// -1 Для того что бы найти первый элемент
	let left = -1;
	let right = arr.length;

	while (right - left > 1) {
		let middle = Math.floor((left + right) / 2);

		if (arr[middle] === target) return middle;
		if (arr[middle] > target) right = middle;
		if (arr[middle] < target) left = middle;
	}
	return -1;
}

console.log(binarySearch(arr, 1));
