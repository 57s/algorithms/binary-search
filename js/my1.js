const arr = [1, 2, 3, 4, 5, 6, 7, 8];

function binarySearch(arr, target) {
	let mid = Math.floor(arr.length / 2);
	let left = arr.slice(0, mid);
	let right = arr.slice(mid);

	if (arr[mid] === target) return true;
	if (mid === 0) return false;
	if (arr[mid] > target) return binarySearch(left, target);
	if (arr[mid] < target) return binarySearch(right, target);
}

console.log(binarySearch(arr, 7));
