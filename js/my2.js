const arr = [1, 2, 3, 4, 5, 6, 7, 8];

function binarySearch(arr, target) {
	let left = 0;
	let right = arr.length - 1;

	while (left <= right) {
		let middle = Math.floor((left + right) / 2);

		if (arr[middle] === target) return true;
		if (arr[middle] > target) right = middle - 1;
		if (arr[middle] < target) left = middle + 1;
	}

	return false;
}

console.log(binarySearch(arr, 5));
